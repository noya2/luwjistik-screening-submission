@extends('layouts.app')

@section('title')
    Create Order | Create Order
@endsection
@section('content')

    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                    <div>
                        <h2 class="main-content-title tx-24 mg-b-5">Create Order</h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{url('/dashboard')}}">Dasboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Create Order</li>
                        </ol>
                    </div>
                </div>

                <div class="row row-sm">
                    <div class="col-12 col-md-8">
                        <div class="card custom-card overflow-hidden">
                            <div class="card-body">
                                @if(session('success'))
                                    <div class="alert alert-success" role="alert">
                                        <button class="btn-close" data-bs-dismiss="alert"></button>
                                        <span>{{session('success')}}</span>
                                    </div>
                                @endif
                                @if(session('error'))
                                    <div class="alert alert-danger" role="alert">
                                        <button class="btn-close" data-bs-dismiss="alert"></button>
                                        <span>{{session('error')}}</span>
                                    </div>
                                @endif

                                    <form id="order" class="needs-validation" method="POST" action="" novalidate>

                                        <div class="wrap_form">
                                            <h3 class="mb-3">Order Information</h3>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Name</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="text" class="form-control" required="" name="cos_name" id="cos_name">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Phone Number</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="numbe" class="form-control" required="" name="cos_number" id="cos_number">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Address</label>
                                                <div class="col-12 col-md-8">
                                                    <textarea type="text" class="form-control" required="" name="cos_add" id="cos_add"></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Postal Code</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="numbe" class="form-control" required="" name="cos_postal" id="cos_postal">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Country</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="text" class="form-control" required="" name="cos_country" id="cos_country">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">City</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="text" class="form-control" required="" name="cos_city" id="cos_city">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">State</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="text" class="form-control" required="" name="cos_state" id="cos_state">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Province</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="text" class="form-control" required="" name="cos_prove" id="cos_prove">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Email</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="email" class="form-control" required="" name="cos_email" id="cos_email">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="wrap_form mt-5">
                                            <h3 class="mb-3">Item & Payment Type</h3>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Length</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="number" class="form-control" name="cos_length" id="cos_length">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Width</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="number" class="form-control" name="cos_width" id="cos_width">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Height</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="number" class="form-control" name="cos_height" id="cos_height">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Weight</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="number" class="form-control" name="cos_weight" id="cos_weight">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Payment Type</label>
                                                <div class="col-12 col-md-8">
                                                    <select id="cos_payment" class="form-select">
                                                        <option>Transfer Bank</option>
                                                        <option>GoPay</option>
                                                        <option>Credit Card</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="wrap_form mt-5">
                                            <h3 class="mb-3">Pickup Information</h3>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Pickup Name</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="text" class="form-control" required="" name="pick_name" id="pick_name">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Pickup Number</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="number" class="form-control" required="" name="pick_number" id="pick_number">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Pickup Address</label>
                                                <div class="col-12 col-md-8">
                                                    <textarea type="text" class="form-control" required="" name="pick_add" id="pick_add"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Pickup Postal</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="number" class="form-control" required="" name="pick_postal" id="pick_postal">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Pickup Country</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="text" class="form-control" required="" name="pick_country" id="pick_country">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Pickup City</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="text" class="form-control" required="" name="pick_city" id="pick_city">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Pickup State</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="text" class="form-control" required="" name="pick_state" id="pick_state">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="user_name" class="col-md-4 col-12 col-form-label">Pickup Province</label>
                                                <div class="col-12 col-md-8">
                                                    <input type="text" class="form-control" required="" name="pick_prove" id="pick_prove">
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group justify-content-end mb-0 row">
                                            <div class="col-12 col-md-12">
                                                <button class="btn btn-primary float-right" type="submit">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                <div id="result" class="text-center"></div>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


@endsection
@section('js')
<script>

    $("#order").submit(function(e) {
    e.preventDefault();
    $.ajax({
        url: 'https://frontend-screening-v1.herokuapp.com/order',
        dataType: 'json',
        type: 'post',
        contentType: 'application/json; charset=utf-8',
        headers: {"Authorization": "f428d380583f81dff5d148c13da798bb9d59e9bf0f6862d137f0a19566d50626"},

        data: JSON.stringify(
            {
                "consignee_name": $('#cos_name').val(),
                "consignee_number": $('#cos_number').val(),
                "consignee_address": $('#cos_add').val(),
                "consignee_postal": $('#cos_postal').val(),
                "consignee_country": $('#cos_country').val(),
                "consignee_city": $('#cos_city').val(),
                "consignee_state": $('#cos_state').val(),
                "consignee_province": $('#cos_prove').val(),
                "consignee_email": $('#cos_email').val(),
                "length": parseInt($('#cos_length').val()),
                "width": parseInt($('#cos_width').val()),
                "height": parseInt($('#cos_height').val()),
                "weight": parseInt($('#cos_weight').val()),
                "payment_type": $('#cos_payment').val(),
                "pickup_contact_name": $('#pick_name').val(),
                "pickup_contact_number": $('#pick_number').val(),
                "pickup_address": $('#pick_add').val(),
                "pickup_postal": $('#pick_postal').val(),
                "pickup_country": $('#pick_country').val(),
                "pickup_city": $('#pick_city').val(),
                "pickup_state": $('#pick_state').val(),
                "pickup_province": $('#pick_prove').val(),
            }),
        processData: false,
        success: function( data){
            window.location = '/dashboard';
            console.log(data)
            // alert(data.success)
        },
        error: function( data){
            console.log( data );
            $('#result').empty().addClass('error').append(data.responseJSON.message);
        }
    });

    });

</script>
@endsection
