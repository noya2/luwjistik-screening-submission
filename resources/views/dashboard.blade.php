@extends('layouts.app')

@section('title')
    Home |
@endsection

@section('css')

@endsection

@section('content')

    <!--Content-->
    <div class="main-content side-content pt-0">
        <div class="container-fluid">
            <div class="inner-body">

                <div class="page-header">
                    <div>
                        <h2 class="main-content-title tx-24 mg-b-5">Dashboard</h2>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                        </ol>
                    </div>
                </div>


                <div class="row row-sm">
                    <div class="wrap_button text-right mb-5">
                        <a class="btn-primary p-3" href="/create-order"><i class="fas fa-plus"></i> Create Order</a>
                    </div>

                    <div class="col-12 col-md-12">
                        <div class="card custom-card overflow-hidden min-height500">
                            <div class="card-header border-bottom-0">
                                <div>
                                    <label class="main-content-label mb-0">List of Orders</label>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table" id="t_data">
                                        <thead>
                                        <tr>
                                            <th>Order ID</th>
                                            <th>Name</th>
                                            <th>Phone Number</th>
                                            <th>Address</th>
                                            <th>Postal Code</th>
                                            <th>Country</th>
                                            <th>City</th>
                                            <th>State</th>
                                            <th>Province</th>
                                            <th>Email</th>
                                            <th>length</th>
                                            <th>Width</th>
                                            <th>Height</th>
                                            <th>Weight</th>
                                            <th>Payment Type</th>
                                            <th>Pickup Name</th>
                                            <th>Pickup Number</th>
                                            <th>Pickup Address</th>
                                            <th>Pickup Postal Code</th>
                                            <th>Pickup Country</th>
                                            <th>Pickup City</th>
                                            <th>Pickup State</th>
                                            <th>Pickup Province</th>
                                            <th>User ID</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>



@endsection

@section('js')

<script type="text/javascript">
    // var table = $('#t_data').DataTable();


    var table = $("#t_data").DataTable({
        orderCellsTop: true,
        filter : true,
        sortable: true,
        // info: true,
        paging: true,
        processing: true,
        serverSide: true,
        ordering : false,
        order : [[0,'asc']],
        responsive:true,

        ajax: {
            url: "https://frontend-screening-v1.herokuapp.com/order",
            dataType: 'json',
            type: 'get',
            contentType: 'application/json; charset=utf-8',
            headers: {"Authorization": "f428d380583f81dff5d148c13da798bb9d59e9bf0f6862d137f0a19566d50626"},
            data : function(data){
                return {
                    draw: data.draw,
                    // start: data.start,
                    // length: data.length,
                    columns:data.columns,
                    order:data.order[0],
                };
            },
        },
        columns: [
            {data:"order_id"},
            {data:"consignee_name"},
            {data:"consignee_number"},
            {data:"consignee_address"},
            {data:"consignee_postal"},
            {data:"consignee_country"},
            {data:"consignee_city"},
            {data:"consignee_state"},
            {data:"consignee_province"},
            {data:"consignee_email"},
            {data:"length"},
            {data:"width"},
            {data:"height"},
            {data:"weight"},
            {data:"payment_type"},
            {data:"pickup_contact_name"},
            {data:"pickup_contact_number"},
            {data:"pickup_address"},
            {data:"pickup_postal"},
            {data:"pickup_country"},
            {data:"pickup_city"},
            {data:"pickup_state"},
            {data:"pickup_province"},
            {data:"user_id"},
            // { data: "id",orderable:false,searchable:false },
        ],

    });
    // $('#t_data_filter input.form-control').on( 'keyup', function () {
    //     table.search( this.value ).draw();
    // } );

</script>

@endsection
