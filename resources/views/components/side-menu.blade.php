<!--Side Menu-->
<div class="main-sidebar main-sidebar-sticky side-menu">
    <div class="sidemenu-logo">
        <a class="main-logo" href="{{url('/')}}">
            <img src="{{asset('images/logo_2.png')}}" class="header-brand-img desktop-logo" alt="logo">

        </a>
    </div>
    <div class="main-sidebar-body">
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{url('/dashboard')}}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="fas fa-home sidemenu-icon"></i>
                    <span class="sidemenu-label">Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{url('/create-order')}}">
                    <span class="shape1"></span>
                    <span class="shape2"></span>
                    <i class="fas fa-plus sidemenu-icon"></i>
                    <span class="sidemenu-label">Create Order</span>
                </a>
            </li>

        </ul>
    </div>
</div>
