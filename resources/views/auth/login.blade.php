<!DOCTYPE html>
<html lang="ID">
<head>
    <meta charset="UTF-8"/>
    <title>Luwjistik - Login</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups" />
    <meta name="keywords" content="Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups" />
    <meta name="generator" content="Luwjistik" />
    <meta name="robots" content="index, follow" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

    <!-- ICON -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css" href="fonts/flaticon/flaticon.css">

    <!--Style-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/theme/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/theme/skin.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/theme/responsive.css')}}">

</head>
<body class="main-body">

<div class="bg-loginx page main-signin-wrapper">
    <main class="form-signin">

        <div class="row signpages text-center">

            <div class="col-md-12">
                <div class="card">
                    <div class="row row-sm">
                        <div class="col-lg-6 col-xl-5 d-none d-lg-block text-center bg-primary details">
                            <div class="pos-absolute wrap_logo">
                                <img src="{{(asset('images/logo_2.png'))}}" class="logo_login" alt="logo">
                            </div>
                        </div>

                        <div class="col-lg-6 col-xl-7 col-xs-12 col-sm-12 login_form ">
                            <div class="container-fluid">
                                <div class="row row-sm">
                                    <div class="card-body mt-2 mb-2">
{{--                                        <form id="login_form" method="POST" >--}}
{{--                                            @csrf--}}
{{--                                            {!! csrf_field() !!}--}}
{{--                                            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}" />--}}
                                            <h5 class="text-left mb-2">Sign In to Your Account</h5>
                                            <p class="mb-4 text-muted tx-13 ml-0 text-left">Welcome back! Please signin to continue.</p>
                                            <div class="form-group text-left">
                                                <label>Email</label>
                                                <input autofocus type="text" name="email_input" class="form-control" id="email_input" placeholder="Enter your email">
                                            </div>
                                            <div class="form-group text-left">
                                                <label>Password</label>
                                                <input required type="password" name="password_input" class="form-control" id="password_input" placeholder="Enter your password">
                                            </div>
                                            <button id="submit" type="submit" class="btn btn-primary w-100">Sign In</button>
{{--                                        </form>--}}

                                        <div id="result" class="mt-4"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </main>
</div>

<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous">
</script>

<script type="text/javascript">
    $( document ).ready(function() {
        // localStorage.removeItem("value_s");
        $("#submit").click(function(event) {
            $.ajax({
                url: 'https://frontend-screening-v1.herokuapp.com/login',
                dataType: 'json',
                type: 'post',
                contentType: 'application/json',
                // cache: false,
                data: JSON.stringify(
                    {
                        // "_token": $('#_token').val(),
                        "email": $('#email_input').val(),
                        "password": $('#password_input').val()
                    }),
                processData: false,
                success: function( data){
                    window.location = '/dashboard';
                    // $('#response pre').html( JSON.stringify( data ) );
                    console.log(data)
                    return localStorage.setItem('value_s', (JSON.stringify(data)));
                },
                error: function( data){
                    console.log( data );
                    $('#result').empty().addClass('error').append(data.responseJSON.message);
                }
            });

        });

    });
    $(document).ready(function () {
        if (localStorage.getItem("value_s") === null) {
            window.location.navigate("/login");
        }else{
            window.location.replace("/dashboard");
        }
    });
</script>

</body>
</html>

