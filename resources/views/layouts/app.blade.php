<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta charset="UTF-8"/>
    <title>Luwijistik Dashboard</title>
    <meta name="description" content="Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups" />
    <meta name="keywords" content="Lorem ipsum is placeholder text commonly used in the graphic, print, and publishing industries for previewing layouts and visual mockups" />
    <meta name="generator" content="Luwjistik" />
    <meta name="robots" content="index, follow" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{asset('images/favicon.ico')}}">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

    <!-- ICON -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" type="text/css" href="fonts/flaticon/flaticon.css">

    <!--Style-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/theme/style.css?v=1.4')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/theme/skin.css?v=1.3')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/theme/responsive.css?v=1.3')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/theme/side-menu.css?v=1.3')}}">

</head>
<body class="wrap_body main-body leftmenu">

@include('components.side-menu')

@include('layouts.header')

@yield('content')
{{--<a href="javascript:" id="return-to-top"><i class="fas fa-chevron-up"></i></a>--}}


@include('layouts.footer')


<script
        src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
        crossorigin="anonymous">
</script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/theme.js')}}"></script>
<script src="{{asset('js/skin.js')}}"></script>

<script src="{{asset('js/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{asset('js/plugins/sidemenu/sidemenu.js')}}"></script>
<script src="{{asset('js/plugins/sidebar/sidebar.js')}}"></script>
<script src="{{asset('js/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('js/plugins/chart.js/Chart.bundle.min.js')}}"></script>

<script src="{{asset('js/plugins/datatable/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/plugins/datatable/dataTables.bootstrap4.min.js')}}"></script>

{{--<script src="{{asset('js/3rd/sticky.js')}}"></script>--}}
<script src="{{asset('js/3rd/custom.js')}}"></script>

@yield('js')

<script>
    $('#logout').click(function () {
        localStorage.removeItem("value_s");
    });
    $(document).ready(function () {
        if (localStorage.getItem("value_s") === null) {
            window.location.replace("/login");
        }else{
            window.location.navigate("/dashboard");
        }

    });
</script>

</body>
</html>
