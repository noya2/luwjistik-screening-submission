<header class="head_style">
   <div class="wrap_header">
      <h5>Hi, {{session('user')}}</h5>
      <a id="logout" href="/logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
   </div>
</header>