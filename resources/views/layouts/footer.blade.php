<!--Footer-->
<div class="main-footer text-center">
    <div class="container">
        <div class="row row-sm">
            <div class="col-md-12">
                <span class="copy">Copyright © <a href="#">Luwijistik</a> 2022. All rights reserved.</span>
            </div>
        </div>
    </div>
</div>

{{--@section('js')--}}
{{--<script type="text/javascript">--}}
{{--    $('#logout').click(function () {--}}
{{--        localStorage.removeItem("value_s");--}}
{{--        alert('click');--}}
{{--    });--}}
{{--</script>--}}
{{--@endsection--}}
