<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserAuth;
//use App\Http\Controllers\Auth\LoginController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//

Route::post("user",[UserAuth::class,'userLogin']);
//Route::view("login", "auth.login" );
Route::view("dashboard", "dashboard" );


Route::get('/login', function () {
    if (session()->has('user')){
        return redirect('dashboard');
    }else{
        return view('auth.login');
    }
});


Route::view("/create-order", "order" );

Route::get('/logout', function () {
    if (session()->has('user')){
        session()->pull('user');
        Auth::logout();
        Session::flush();
    }
    return redirect('login');
});

Route::get('ajaxRequest', [AjaxController::class, 'ajaxRequest']);
Route::post('ajaxRequest', [AjaxController::class, 'ajaxRequestPost'])->name('ajaxRequest.post');
